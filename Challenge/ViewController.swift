//
//  ViewController.swift
//  Challenge
//
//  Created by Роман Хоменко on 02.04.2022.
//

import UIKit

class ViewController: UITableViewController {
    var shopingList: [String] = []
    var rightBarButtonItems: [UIBarButtonItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Shoping List"
        
        createRightBarButtonItems()
        
//        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addPurchase))
        navigationItem.rightBarButtonItems = rightBarButtonItems
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(clearShopingList))
    }
}

extension ViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shopingList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Shop", for: indexPath)
        cell.textLabel?.text = shopingList[indexPath.row]
        cell.textLabel?.font = UIFont(descriptor: UIFontDescriptor(name: "System", size: 15), size: 15)
        
        return cell
    }
}

extension ViewController {
    func createRightBarButtonItems() {
        rightBarButtonItems.append(UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addPurchase)))
        rightBarButtonItems.append(UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(sharePurchaseList)))
    }
}

extension ViewController {
    @objc func addPurchase() {
        let alert = UIAlertController(title: "Add purchase in list", message: nil, preferredStyle: .alert)
        alert.addTextField()
        
        let addAction = UIAlertAction(title: "Add", style: .default) { [weak self, weak alert] _ in
            guard let purchase = alert?.textFields?[0].text else { return }
            self?.add(purchase)
        }
        
        alert.addAction(addAction)
        present(alert, animated: true)
    }
    
    func add(_ purchase: String) {
        let indexPath = IndexPath(row: 0, section: 0)
        shopingList.insert(purchase, at: 0)
        
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    @objc func clearShopingList() {
        shopingList = []
        tableView.reloadData()
    }
    
    @objc func sharePurchaseList() {
        let list = shopingList.joined(separator: "\n")
        
        let activity = UIActivityViewController(activityItems: [list], applicationActivities: [])
        activity.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(activity, animated: true)
    }
}
